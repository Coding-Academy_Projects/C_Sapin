#include <stdio.h>
#include <stdlib.h>

void    sapin(int size);

int     main(int argc, char **argv)
{
    
    int nombre;

    if (argv[1] != NULL && argv[2] == NULL)
    {
        nombre = atoi(argv[1]);
        if (nombre <= 0)
        {
            printf("Ce programme refuse de dessiner un sapin en cas d'utilisation de chiffres négatifs, de lettres, ou de 0. Merci de votre compréhension.\n");
        }
        else
        {
            sapin(nombre);
        }
    }
    else
    {
        printf("Le but c'est d'entrer juste un nombre en paramètre, hein.\n");
    }
    return 0;
    

    //int nombre;

    //nombre = atoi(argv[1]);
    //sapin(nombre);
    return 0;
}