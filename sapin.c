#include <unistd.h>

int     my_putchar(char c)
{
    write(1, &c, 1);
}

/*
** Calculer le nombre d'étoiles sur la dernière line.
*/
int     my_size(int size)
{
    int floor_number = 1;
    int max_size = 7;
    int star_count = 6;

    while (floor_number < size)
    {
        // Si on passe d'un étage pair à impair.
        if (floor_number % 2 == 0)
        {
            star_count =  star_count + 2;
            max_size = max_size + star_count;
        }
        // Si on passe d'un étage impair à pair.
        else
        {
            max_size = max_size + star_count;
        }
        floor_number++;
    }
    return  max_size;
}

/*
** Dessiner le sapin.
*/
void    my_stars(int size)
{
    int floor_number = 1;
    int star = 0;
    int space = (my_size(size) / 2) - star;
    int line_per_floor = floor_number + 3;
    int line = 1;
    int floor_jump_star = 0;
    int floor_jump_space = 0;
    int i = 0;
    int j = 0;

    while (floor_number <= size)
    {
        while (line <= line_per_floor)
        {
            while (j < space)
            {
                my_putchar(32);
                j++;
            }
            while (i <= star)
            {
                my_putchar(42);
                i++;
            }
            my_putchar('\n');
            star = star + 2;
            space = space - 1;
            line++;
            i = 0;
            j = 0;
        }
        // Si on passe d'un étage pair à impair, on retire 2 étoiles et ajoute un space de plus.
        if (floor_number % 2 != 0)
        {
            floor_jump_star = floor_jump_star + 2;
            floor_jump_space = floor_jump_space + 1;
            star = (star - 2) - floor_jump_star;
            space = (space + 1) + floor_jump_space;

        }
        // Si l'étage est pair et qu'on passe sur un étage impair on ne touche pas aux sauts.
        else
        {
            star = (star - 2) - floor_jump_star;
            space = (space + 1) + floor_jump_space;
        }
        floor_number++;
        line_per_floor++;
        line = 1;
    }
}

/*
** Déterminer et dessiner le tronc.
*/
void     my_trunk(int size)
{
    int floor_number = 1;
    int pipes = 1;
    int space;
    int height = 1;
    int column_counter = 0;
    int line_counter = 0;

    // Calcule du nombre de colonnes.
    while (floor_number <= size)
    {
        // Quand on passe d'impair à pair, on ajoute 2 au nombre de colonne.
        if (floor_number % 2 == 0)
        {
            pipes = size + 1;
        }
        else
        {
            pipes = size;
        }
        floor_number++;
    }

    space = (my_size(size) / 2) - (pipes / 2);

    // Dessiner le tronc.
    while (height <= size)
    {
        while (line_counter < space)
        {
            my_putchar(32);
            line_counter++;
        }

        while (column_counter < pipes)
        {
            my_putchar(124);
            column_counter++;
        }
    my_putchar('\n');
    height++;
    column_counter = 0;
    line_counter = 0;
    }
}

void    sapin(int size)
{
    //if (size > 0)
    //{
        my_stars(size);
        my_trunk(size);
    //}
    //else
    //{
    //    write(1, "The argument must be positive.\n", 31);
    //}
}